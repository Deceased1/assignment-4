
/*
 * Takes an input-string, provided by the user, and tests whether a given
 * string is an anagram of another given string.
 */
const testAnagram = (stringOne, stringTwo)=> {
    // If the lengths of the strings are different, it cannot be true.
    if ( stringOne.length !== stringTwo.length ) {
        return false;
    }

    // Sort both strings after converting them to lowercased characters.
    let sortedOne = stringOne.toLowerCase().split( '' ).sort();
    let sortedTwo = stringTwo.toLowerCase().split( '' ).sort();

    // Compare the sorted strings
    for ( let i = 0; i < sortedOne.length; i++ ) {

        // If the characters don't align, it cannot be an anagram.
        if ( sortedOne[ i ] !== sortedTwo[ i ] ) {
            return false;
        }
    }
    return true;
};

const run = ()=> {
    const inputOne = process.argv[ 2 ];
    const inputTwo = process.argv[ 3 ];
    // If input was not provided, notify the user.
    if ( !inputOne ) {
        console.log( 'Please specify the first string to test, e.g. "hello"' );
        return;
    }
    if ( !inputTwo ) {
        console.log( 'Please specify the second string to test, e.g. "ohell"' );
        return;
    }

    // Display the result.
    let isAnagram = testAnagram( inputOne, inputTwo );
    console.log( `The given string ${ (isAnagram) ? 'is' : 'is not' } an anagram.` );
};

run();