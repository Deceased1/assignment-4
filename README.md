
# Assignment 4

This assignment takes two given strings and runs a test against them to see whether the second string provided is an anagram of the first string or not.

> a word, phrase, or name formed by rearranging the letters of another, such as spar, formed from rasp.

## Running the script

To execute this script, Node JS needs to exist on the target machine. Once Node JS is installed, the script can be execute with:

```
node index.js "first-string-to-test" "second-string-to-test"
```
Replace ```first-string-to-test``` and ```second-string-to-test``` with the desired strings to test, e.g. ```anagram``` and ```nag a ram```